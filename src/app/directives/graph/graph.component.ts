import {
  Component, OnInit, Input, OnChanges, ViewChild, ViewChildren, AfterViewInit,
  AfterViewChecked
} from '@angular/core';
import {GraphData} from "../../domain/graph_data";
import {ChartComponent} from "angular2-chartjs";

@Component({
  selector: 'app-ng-graph',
  templateUrl: 'graph.component.html',
  styleUrls: ['graph.component.css']
})
export class GraphComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked {


  @Input()
  graphData : GraphData;
  @ViewChild(ChartComponent)
  public Chart : ChartComponent;

  public type = "line";
  public data = {
    labels: [""],
    datasets: [
      {
        label: "Loading",
        data: []
      }
    ]
  };
  public options = {
    responsive: true,
    maintainAspectRatio: false,

    pan: {
      // Boolean to enable panning
      enabled: true,

      // Panning directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow panning in the y direction
      mode: 'xy'
    },

    // Container for zoom options
    zoom: {
      // Boolean to enable zooming
      enabled: true,

      // Zooming directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow zooming in the y direction
      mode: 'xy',
    }
  };
  constructor() {

  }

  ngOnInit() {

  }

  ngOnChanges(){
    this.type = this.graphData.type;
    this.data = {
      labels: this.graphData.data_labels,
      datasets: [
        {
          label: this.graphData.label,
          data: this.graphData.data_points
        }
      ]
    };
    console.log("After Change")
  }

  ngAfterViewInit(): void {
    console.log("Ater View Init")
  }

  ngAfterViewChecked(): void {

  }

}
