import {Component, OnInit, Input} from "@angular/core";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  @Input()
  private status : boolean;
  private icon : string = "";
  private className = "";
  private action = "";
  constructor() { }

  ngOnInit() {
    console.log("==== Alert Status Received: " + this.status + " ====");
    if(this.status){
      this.icon = "fa fa-check";
      this.className = "alert alert-success alert-dismissible";
      this.action = "Action Complete"
    } else {
      this.icon = "fa fa-ban";
      this.className = "alert alert-danger alert-dismissible";
      this.action = "Action Failed"

    }
  }
}
