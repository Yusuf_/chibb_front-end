import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {StompService} from "./services/temperature-socket-service";
import {AppComponent} from "./app.component";
import {HeaderComponent} from "./directives/header/header.component";
import {GraphComponent} from "./directives/graph/graph.component";
import {ChartModule} from "angular2-chartjs";
import {SidebarComponent} from "./directives/sidebar/sidebar.component";
import {HomeComponent} from "./pages/home/home.component";
import {FooterComponent} from "./directives/footer/footer.component";
import {routes} from "./routes/routes";
import {RouterModule} from "@angular/router";
import {TemperatureComponent} from "./pages/temperature/temperature.component";
import {TemperatureAPIService} from "./services/temperature_service";
import {ServiceComponent} from "./pages/services/services.component";
import {DiscoveryService} from "./services/discovery_service";
import {SensorNodeService} from "./services/node_service";
import {NodeDetailComponent} from "./pages/service-detail/service-detail.component";
import {RegisterSensorNodeComponent} from "./pages/register-sensor-node/register-sensor-node.component";
import {SensorNodeDetailComponent} from "./pages/sensor-node-detail/sensor-node-detail.component";
import {AlertComponent} from "./directives/alert/alert.component";
import {NotFoundComponent} from "./pages/not-found/not-found.component";
import {ChartsComponent} from "./pages/charts/charts.component";
import {CanvasComponent} from "./pages/canvas/canvas.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GraphComponent,
    SidebarComponent,
    HomeComponent,
    FooterComponent,
    TemperatureComponent,
    ServiceComponent,
    NodeDetailComponent,
    RegisterSensorNodeComponent,
    SensorNodeDetailComponent,
    AlertComponent,
    NotFoundComponent,
    ChartsComponent,
    CanvasComponent
  ],

  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartModule,
  ],
  providers: [StompService, TemperatureAPIService, DiscoveryService, SensorNodeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
