import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {Http, Headers, Response} from "@angular/http";
import {Observable} from "rxjs";
import {SensorService} from "../domain/sensor_service";
/**
 * Created by Yusuf on 14-Mar-17.
 */
@Injectable()
export class DiscoveryService {
  serverUrl = environment.server.discovery.url;
  private temperatureUrl = "http://" + this.serverUrl;

  constructor (private http: Http) {}

  getData(): Observable<SensorService[]> {
    let url = this.temperatureUrl;;
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    console.log("Requesting: " + url);
    return  this.http.get(url, headers)
        .map(this.extractData)
        .catch(this.handleError)
  }

  public extractData(res: Response){
    let body = res.json();
    let services : SensorService[] = [];

    body.application.forEach(function (item) {
      let service = new SensorService();

      service.name = item.name;
      item.instance.forEach(function (item2) {
        service.instance_id = item2.instanceId;
        service.ip_address = item2.homePageUrl;
        service.status = item2.status;
      });
      services.push(service);
    });

    return services;
  }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
