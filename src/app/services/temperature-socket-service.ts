/**
 * Created by Yusuf on 18-Feb-17.
 */
import {Injectable} from "@angular/core";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import "stompjs";
import {environment} from "../../environments/environment";

declare let Stomp:any;

@Injectable()
export class StompService {

  private stompClient;
  private stompSubject : Subject<any> = new Subject<any>();
  private socketUrl = environment.server.temperature.socket;

  public connect() : void {
    let self = this;
    let webSocket = new WebSocket(this.socketUrl);
    this.stompClient = Stomp.over(webSocket);
    // this.stompSubject = new Subject<any>();
    this.stompClient.connect({}, function () {
      self.stompClient.subscribe('/topic/sensor-data', function (stompResponse) {
        // stompResponse = {command, headers, body with JSON
        // reflecting the object returned by Spring framework}
        self.stompSubject.next(JSON.parse(stompResponse.body));
      });
    });
  }

  public disconnect() : void {
    this.stompSubject.complete();
    this.stompSubject = new Subject<any>()
    this.stompClient.disconnect();
  }

  public getObservable() : Observable<any> {
    return this.stompSubject.asObservable();
  }
}
