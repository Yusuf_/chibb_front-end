import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {SensorNode} from "../domain/sensor_node";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {SensorReading} from "../domain/sensor_reading";
import * as moment from "moment";
import {Router} from "@angular/router";

/**
 * Created by Yusuf on 18-Mar-17.
 */
@Injectable()
export class SensorNodeService {
  serverUrl = environment.server.sensor_nodes.url;
  private sensorNodeURL = this.serverUrl + "/sensorNodes/";

  constructor (private http: Http, private router: Router) {}

  getData(): Observable<SensorNode[]> {
    let url = this.sensorNodeURL;
    console.log("Requesting: " + url);
    return this.http.get(url)
      .map(this.extractGETNodes)
      .catch(this.handleError)
  }

  getActiveNodes() : Observable<SensorNode[]>{
    let url = this.sensorNodeURL + '/sensors/active';
    console.log("Requesting: " + url);
    return this.http.get(url)
      .map((res: Response )=> {
        console.log(res);
        return res.json();
      })
      .catch(this.handleError)
  }

  getSensorNodeById(id: number) : Observable<SensorNode>{
    let url = this.sensorNodeURL + id;
    console.log("Requesting: 2 " + url);
    return this.http.get(url)
      .map((res: Response )=> {
      console.log("==== Inside Get By Id ====");
      console.log(res);
        return res.json();
      })
      .catch(this.handleError)
  }

  getLastFiveMinuteDataById(node: SensorNode) : Observable<SensorReading[]>{
    let timestamp = new Date();
    let fiveMinutesAgo = moment(timestamp).subtract(5, "m").format("YYYY-MM-DD-HH:mm:ss");

    let url = this.serverUrl + "/sensors/data/" + node.id + "?date=" + fiveMinutesAgo;
    console.log("Requesting: " + url);

    return this.http.get(url)
      .map((res: Response )=> {
        return res.json();
      })
      .catch(this.handleError)
  }

  getDataFromNode(node: SensorNode, datePeriod : moment.unitOfTime.DurationConstructor) : Observable<SensorReading[]>{
    let timestamp = new Date();
    let period = moment(timestamp).subtract(1, datePeriod).format("YYYY-MM-DD-HH:mm:ss");

    let url = this.serverUrl + "/sensors/data/" + node.id + "?date=" + period;
    console.log("Requesting: " + url);

    return this.http.get(url)
      .map((res: Response )=> {
        return res.json();
      })
      .catch(this.handleError)
  }

  connectSensorNodeById(id : string){
    let url = this.serverUrl +  "/sensors/connect/" + id;
    console.log("Requesting: " + url);
    return this.http.post(url, null, null)
      .map((res: Response )=> {
        return res.json();
      })
      .catch(this.handleError)
  }

  disconnectSensorNodeById(id : string){
    let url = this.serverUrl +  "/sensors/disconnect/" + id;
    console.log("Requesting: " + url);
    return this.http.post(url, null, null)
      .map((res: Response )=> {
        return res.json();
      })
      .catch(this.handleError)
  }

  updateSensorNode(node: SensorNode) : Observable<any>{
    let url = this.serverUrl + "/sensors/update/" + node.id;
    console.log("Requesting: " + url);
    let bodyString = JSON.stringify(node); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers });
    return this.http.put(url, bodyString, options)
      .map((res: Response )=> {
        return res.json();
      })
      .catch(this.handleError)
  }

  addSensorNodes(sensorNode: SensorNode) : Observable<SensorNode> {
    let bodyString = JSON.stringify(sensorNode); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.sensorNodeURL, bodyString, options)
      .map((res: Response )=> {
      return res.json();
      })
      .catch(this.handleError);
  }

  private extractGETNodes(res: Response) {
    let body = res.json();
    let sensorNodes = [];

    body._embedded.sensorNodes.forEach(function (item) {
      let sensorNode = new SensorNode();
      sensorNode.ip = item.ip;
      sensorNode.type = item.type;
      sensorNode.name = item.name;
      sensorNode.topic = item.topic;
      sensorNode.status = item.status;
      sensorNode.connectionStatus = item.connectionStatus;
      sensorNode.id = item.id;
      sensorNodes.push(sensorNode);
    });
    return sensorNodes;
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure

    let errMsg: string;
    console.log(error)
    if(error.status == 404){
      errMsg = "Not Found";
      console.log("Inside router");
      location.replace('404');

    }
/*    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }*/
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
