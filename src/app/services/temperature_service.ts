import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {SensorData2} from "../domain/sensor_data2";
import {SensorReading} from "../domain/sensor_reading";
/**
 * Created by Yusuf on 02-Mar-17.
 */
@Injectable()
export class TemperatureAPIService {
  serverUrl = environment.server.temperature.api;
  private temperatureUrl =  this.serverUrl + "/sensors/history/?date=";

  constructor (private http: Http) {}

  getData(period: string): Observable<SensorData2> {
    let url = this.temperatureUrl + period;
    console.log("Requesting: " + url);
    return this.http.get(url)
      .map(this.extractData2)
      .catch(this.handleError)
  }

  private extractData2(res: Response) {
    let sd = new SensorData2();
    let body = res.json();
    let readings = [];

    if(body.length > 0){
      body.forEach(function (item) {
        sd.location = item.location;
        sd.battery = item.battery;
        sd.timestamp = item.timestamp;

        item.reading.forEach(function (item2) {
          let type = item2.type;
          let reading = Number.parseFloat(item2.reading);
          let unit = item2.unit;
          let timestamp = item2.timestamp;
          let sensorReading = new SensorReading(type,reading,unit,timestamp);
          readings.push(sensorReading);
        });
        sd.reading = readings;
      });
      return sd;
    } else {
      return new SensorData2()
    }
  }
  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
