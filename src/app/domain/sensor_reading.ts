/**
 * Created by Yusuf on 11-Mar-17.
 */
export class SensorReading {
  public type : string;
  public reading : number;
  public unit : string;
  public timestamp : string;

  constructor(type: string, reading: number, unit: string, timestamp: string) {
    this.type = type;
    this.reading = reading;
    this.unit = unit;
    this.timestamp = timestamp;
  }
}
