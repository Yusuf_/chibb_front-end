import {SensorReading} from "./sensor_reading";
/**
 * Created by Yusuf on 11-Mar-17.
 */
export class SensorData2 {
  public location : string = "Somewhere...";
  public reading : Array<SensorReading> = [];
  public battery: number = 0;
  public timestamp: string = "Future...";
}
