/**
 * Created by Yusuf on 02-Mar-17.
 */
export class SensorData {
  public dataLabels : string[] = [];
  public dataPoints : number[] = [];

  constructor(sensorData: number[], sensorLabels: string[]){
    this.dataLabels = sensorLabels;
    this.dataPoints = sensorData;
  }

}
