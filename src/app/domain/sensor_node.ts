import {SensorNodeFailure} from "./sensor_failure";
/**
 * Created by Yusuf on 18-Mar-17.
 */
export class SensorNode {
  public name : string;
  public ip : string;
  public topic : string;
  public type : string;
  public status : string;
  public connectionStatus : string;
  public id : string;
  public failures : Array<SensorNodeFailure> = []

  public static selectStatusClass(node: SensorNode) : string{
    if(node.status == "DOWN"){
      return "label label-danger";
    }else if (node.status == "INTERMITTENT_FAILURE"){
      return "label label-warning";
    }else {
      return "label label-success";
    }
  }

  public static selectConnectionClass(node: SensorNode) : string{
    return  node.connectionStatus == "CONNECTED" ? "label label-success" : "label label-danger"
  }
}
