/**
 * Created by Yusuf on 01-Mar-17.
 */
export class GraphData {
  public size : number;
  public label : string;
  public data_labels : string[];
  public data_points : number[];
  public type: string;

  constructor(){
    this.label = "Loading...";
    this.data_labels = ["First", "Second"];
    this.data_points = [1,2];
    this.type="line"
  }
}
