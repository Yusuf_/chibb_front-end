/**
 * Created by Yusuf on 31-May-17.
 */
export class SensorNodeFailure {
  public timestamp : string;
  public owner : number;
  public cause :string;


  constructor(timestamp: string, owner: number, cause: string) {
    this.timestamp = timestamp;
    this.owner = owner;
    this.cause = cause;
  }
}
