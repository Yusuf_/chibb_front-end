/**
 * Created by Yusuf on 14-Mar-17.
 */
export class SensorService {
  public name :string;
  public ip_address : string;
  public status : string;
  public instance_id : string;
}
