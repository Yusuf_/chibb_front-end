import {Routes} from "@angular/router";
import {HomeComponent} from "../pages/home/home.component";
import {TemperatureComponent} from "../pages/temperature/temperature.component";
import {RegisterSensorNodeComponent} from "../pages/register-sensor-node/register-sensor-node.component";
import {SensorNodeDetailComponent} from "../pages/sensor-node-detail/sensor-node-detail.component";
import {NotFoundComponent} from "../pages/not-found/not-found.component";
import {ChartsComponent} from "../pages/charts/charts.component";
import {CanvasComponent} from "../pages/canvas/canvas.component";
/**
 * Created by Yusuf on 01-Mar-17.
 */
export const routes: Routes = [
  { path : '', component: HomeComponent},
  { path : 'temperature', component : TemperatureComponent},
  { path : 'nodes', component : RegisterSensorNodeComponent},
  { path : 'nodes/new', component : RegisterSensorNodeComponent},
  { path : 'nodes/:id', component : SensorNodeDetailComponent},
  { path : '404', component : NotFoundComponent},
  { path : 'charts', component : ChartsComponent},
  { path : 'canvas', component : CanvasComponent}
];
