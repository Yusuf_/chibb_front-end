import { Component, OnInit } from '@angular/core';
import {DiscoveryService} from "../../services/discovery_service";
import {SensorService} from "../../domain/sensor_service";

@Component({
  selector: 'app-node',
  templateUrl: 'services.component.html',
  styleUrls: ['services.component.css']
})
export class ServiceComponent implements OnInit {

  services : SensorService[] = [];
  constructor(private discoveryService: DiscoveryService) {

  }

  ngOnInit() {
    this.discoveryService.getData().subscribe(
      service => {
        console.log(service);
         this.services = service;
    });
  };
}
