import {Component, OnInit, OnDestroy, ViewChildren, QueryList} from "@angular/core";
import {ChartComponent} from "angular2-chartjs";
import * as moment from "moment";
import "hammerjs";
import "chartjs-plugin-zoom";
import {SensorNode} from "../../domain/sensor_node";
import {SensorNodeService} from "../../services/node_service";
import unitOfTime = moment.unitOfTime;

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit, OnDestroy {

  @ViewChildren(ChartComponent) chartComponent: QueryList<ChartComponent>;
  public timestamp = new Date();
  public subscriptions = [];
  public sensorNodes : any[] = [];
  public unit:moment.unitOfTime.DurationConstructor = 'hour';
  public sensorNode1: SensorNode = new SensorNode();
  public sensorNode2: SensorNode = new SensorNode();
  public period: any;
  public periods = ['Hour', 'Day', 'Week', 'Month', 'Year'];
  type = 'bubble';
  data = {
    labels: [],
    datasets: [
      {
        label: 'Scatter Dataset',
        data: [],
        backgroundColor:"#1bccff",
      },
      {
        label: 'Scatter Dataset 2',
        data: [],
        backgroundColor:"#ff7711",
      }
    ]
  };
  options = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: true,
          autoSkipPadding: 20,
        },
        type: 'time',
        time: {
          displayFormats: {
            millisecond: 'HH:mm:ss',
            second: 'HH:mm:ss',
            minute: 'MMM-DD HH:mm:ss',
            hour: 'MMM-DD HH:mm:ss',
            day: 'MMM DD',
            week: 'MMM DD',
            month: 'MMM DD',
            quarter: 'MMM DD',
            year: 'MMM DD',
          }
        }
      }],
    }
  };

  bar_type = 'bar';
  bar_data = {
    labels: [],
    datasets: [
      {
        label: 'Bar Chart',
        data: [],
        backgroundColor:"#1bccff",
      }
    ]
  };
  bar_options = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: true,
          autoSkipPadding: 20,
        },

      }],
    },
    pan: {
      // Boolean to enable panning
      enabled: true,

      // Panning directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow panning in the y direction
      mode: 'xy'
    },

    // Container for zoom options
    zoom: {
      enabled: true,
      mode: 'x',
      sensitivity: 3,
      limits: {
        max: 10,
        min: 0.5
      }
    }
  };

  polar_type = 'polarArea';
  polar_data = {
    datasets: [{
      data: [],
      backgroundColor: [],
      label: 'Data Sensor Nodes' // for legend
    }],
    labels: []
  };
  polar_options = {
    maintainAspectRatio : false,
    responsive: true,
    elements: {
      arc: {
        borderColor: "#000000"
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: true,
          autoSkipPadding: 10,
        },

      }],
    }
  };


  constructor(private sensorNodeService : SensorNodeService) {

  }

  generateRandomData(){
    let labels = [];
    let points : number[] = [];
    let subscription = this.sensorNodeService.getDataFromNode(this.sensorNode1,this.period).subscribe(data => {
      console.log(data);
      data.forEach(function (item) {
        labels.push(item.timestamp);
        points.push(item.reading);
      });
      console.log(points);
      this.bar_data.datasets[0].data = points;
      this.bar_data.labels = labels;
      this.chartComponent.forEach(function (item) {
        item.chart.update();
      });
    });
    this.subscriptions.push(subscription);
    window.scrollTo(0,1000);

  }

  createBubblePlot(){
    let points  = [];
    let points2  = [];
    let subscription = this.sensorNodeService.getDataFromNode(this.sensorNode1,this.period).subscribe(data => {
      console.log(data);
      for(let i = 0; i < data.length; i++){
        let point = {
          x: moment(data[i].timestamp,'YYYY-MM-DD HH:mm:ss'),
          y: data[i].reading,
          r : 5
        };
        points.push(point);
      }

      this.data.datasets[0].data = points;
      this.data.datasets[0].label = this.sensorNode1.name;
      this.chartComponent.forEach(function (item) {
        item.chart.update();
      });
    });

    let subscription2 = this.sensorNodeService.getDataFromNode(this.sensorNode2,this.period).subscribe(data => {
      console.log(data);
      for(let i = 0; i < data.length; i++){
        let point = {
          x: moment(data[i].timestamp,'YYYY-MM-DD HH:mm:ss'),
          y: data[i].reading,
          r : 5
        };
        points2.push(point);
      }
      this.data.datasets[1].data = points2;
      this.data.datasets[1].label = this.sensorNode2.name;
      this.chartComponent.forEach(function (item) {
        item.chart.update();
      });
    });

    this.subscriptions.push(subscription);
    this.subscriptions.push(subscription2);
    window.scrollTo(0,400)
  }

  createPolarChart(){
    let labels = [];
    let values = [];

    let vm = this;
    for(let i = 0; i < this.sensorNodes.length; i++){
      let node = this.sensorNodes[i];
      let sub =  vm.sensorNodeService.getDataFromNode(node,this.period).subscribe(data => {
        labels.push(node.name);
        values.push(data.length);
        this.subscriptions.push(sub);
        this.polar_data.datasets[0].data = values;
        this.polar_data.labels = labels;
        this.polar_data.datasets[0].backgroundColor.push(this.getRandomColor());
        this.chartComponent.forEach(function (item) {
          item.chart.update();
        });
      });

    }


    window.scrollTo(0,4500)
  }

  getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++ ) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

  ngOnInit() {
    let subscription = this.sensorNodeService.getData().subscribe(data => {
      console.log(data);
      this.sensorNodes = data;
    });
    this.subscriptions.push(subscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(function (sub) {
      sub.unsubscribe();
    })
  }

}
