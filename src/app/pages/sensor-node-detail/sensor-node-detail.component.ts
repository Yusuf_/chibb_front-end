import {Component, OnInit, OnDestroy, ViewChild} from "@angular/core";
import {SensorNode} from "../../domain/sensor_node";
import {SensorReading} from "../../domain/sensor_reading";
import {SensorNodeService} from "../../services/node_service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {StompService} from "../../services/temperature-socket-service";
import {ChartComponent} from "angular2-chartjs";
let CircularBuffer = require("circular-buffer");

@Component({
  selector: 'app-sensor-node-detail',
  templateUrl: './sensor-node-detail.component.html',
  styleUrls: ['./sensor-node-detail.component.css']
})
export class SensorNodeDetailComponent implements OnInit, OnDestroy {
  private node : SensorNode;
  private activeNodes : SensorNode[] = [];
  private editNode : SensorNode = new SensorNode();
  private visibility : boolean = false;
  private visibilityChart : boolean = false;
  private visibilityAlertBox : boolean = false;
  private visibilityFailures : boolean = false;
  private edit : boolean = false;
  private connectionStatus : boolean = false;
  private subscriptions : Subscription[] = [];
  private sensorReadings : SensorReading[] = [];
  private sensorId = "";
  private dataBuffer = new CircularBuffer(5);
  private labelBuffer = new CircularBuffer(5);

  @ViewChild(ChartComponent)
  public Chart : ChartComponent;

  type = 'line';
  data = {
    labels: [],
    datasets: [
      {
        backgroundColor:"#ff7711",
        label: 'Sensor Data',
        fill: true,
        data: [],
      }
    ]
  };
  options = {
    responsive: true,
    maintainAspectRatio: false
  };

  constructor(private sensorNodeService : SensorNodeService,
              private route: ActivatedRoute,
              private router: Router,
              private stompService: StompService) { }

  ngOnInit() {

    let sub = this.route.params.subscribe(params => {
      let id = params['id'];
      this.sensorId = id;
      let sub = this.sensorNodeService.getSensorNodeById(id).subscribe(data =>{
        console.log("==== Received Node ====");
        console.log(data);
        if(data == null){
          this.router.navigate(['404']);
        }else {
          this.node = data;
          let sub = this.sensorNodeService.getLastFiveMinuteDataById(this.node).subscribe(data => {
            this.sensorReadings = data;
          });
          this.subscriptions.push(sub);
        }
      });
      this.subscriptions.push(sub);
    });
    this.subscriptions.push(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(function (sub) {
      sub.unsubscribe();
    })
  }

  public toggleData(){
    console.log("Status " + this.visibility);
    let sub = this.sensorNodeService.getLastFiveMinuteDataById(this.node).subscribe(data => {
      this.sensorReadings = data;
    });

    this.subscriptions.push(sub);
    this.visibility = !this.visibility;
  }

  public toggleEdit(){
    this.editNode = JSON.parse(JSON.stringify(this.node));
    this.edit = !this.edit;
  }

  public toggleFailures(){
    this.visibilityFailures = !this.visibilityFailures;
  }

  public cancelEdit(){
    this.editNode = new SensorNode();
    this.edit = false;
  }

  public saveChanges(){
    /*this.disconnect();*/
    let sub = this.sensorNodeService.updateSensorNode(this.editNode).subscribe(data => {
      console.log("==== UPDATE RESPONSE " + JSON.stringify(data) + " ====");
      this.connectionAction(data.status);
      this.refreshSensorNode();
    });
    this.edit = false;
    this.subscriptions.push(sub);
  }

  public connect(){
    let sub = this.sensorNodeService.connectSensorNodeById(this.node.id).subscribe(data => {
      this.connectionAction(data.status);
      this.connectToSocket();
    });
    this.subscriptions.push(sub);
  }

  public disconnect(){
    let sub = this.sensorNodeService.disconnectSensorNodeById(this.node.id).subscribe(data => {
      this.connectionAction(data.status);
    });
    this.visibilityChart = !this.visibilityChart
    this.stompService.disconnect();

    this.subscriptions.push(sub);
  }

  public connectionAction(data : boolean){
    console.log("==== Alert Status Sending: " + data + " ====");
    this.visibilityAlertBox = true;
    this.connectionStatus = data;
    this.refreshSensorNode();
    setTimeout(() => {
      console.log("==== CONNECTING STATUS " + this.connectionStatus + " ====");
      this.visibilityAlertBox = false;
      this.connectionStatus = false;
    }, 5000);
  }

  public selectStatusClass(node: SensorNode) : string{
    return SensorNode.selectStatusClass(node);
  }

  public selectConnectionClass(node: SensorNode) : string{
    return  SensorNode.selectConnectionClass(node);
  }

  public refreshSensorNode(){
    let sub = this.sensorNodeService.getSensorNodeById(Number.parseInt(this.node.id)).subscribe(data =>{
      this.node = data;
    });
    this.subscriptions.push(sub);
  }

  public toggleChart(){
    this.visibilityChart = !this.visibilityChart
  }

  public connectToSocket(){
    this.stompService.connect();
    this.stompService.getObservable().subscribe(payload => {
      let data = this.dataBuffer;
      let labels = this.labelBuffer;
      for(let i = 0; i < payload.reading.length; i++){
        console.log('==== Received ====');

        console.log(payload.reading[i]);
        let label = payload.reading[i].timestamp;
        let reading = Number.parseFloat(payload.reading[i].reading);
        this.dataBuffer.enq(reading);
        this.labelBuffer.enq(label);
        this.data.datasets[0].data = this.dataBuffer.toarray().reverse();
        this.data.labels = this.labelBuffer.toarray().reverse();
      }

      this.dataBuffer = data;
      this.labelBuffer = labels;
      this.Chart.chart.update();
    });
  }
}
