import {Component, OnInit, OnDestroy} from "@angular/core";
import {SensorNode} from "../../domain/sensor_node";
import {Subscription} from "rxjs";
import {SensorNodeService} from "../../services/node_service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sensor-nodes',
  templateUrl: 'register-sensor-node.component.html',
  styleUrls: ['register-sensor-node.component.css']
})
export class RegisterSensorNodeComponent implements OnInit, OnDestroy {

  private sensorDataTypes : string[] = ["TEMPERATURE", "DENSITY", "PROXIMITY", "LIGHT", "PRESSURE"];
  private sensorNode = new SensorNode();
  private subscriptions : Subscription[] = [];
  private node : SensorNode;
  private submitted = false;
  constructor(private sensorNodeService : SensorNodeService, private router: Router) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(function (sub) {
      sub.unsubscribe();
    })
  }

  onSave(){
    console.log(this.sensorNode);
    let sub = this.sensorNodeService.addSensorNodes(this.sensorNode).subscribe(data =>{
      this.node = data;
      this.router.navigate(['/nodes',this.node.id]);
      this.submitted = true;
    });
    this.subscriptions.push(sub);
  }

  createNew(){
    this.node = null;
    this.submitted = false
  }

}
