import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-node-detail',
  templateUrl: 'service-detail.component.html',
  styleUrls: ['service-detail.component.css']
})
export class NodeDetailComponent implements OnInit, OnDestroy {
  private id : string;
  private sub;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log("ID: " + this.id)
    });
  }

  ngOnDestroy() {
    // Clean sub to avoid memory leak
    this.sub.unsubscribe();
  }

}
