import {Component, OnInit, OnDestroy} from "@angular/core";
import {SensorNodeService} from "../../services/node_service";
import {SensorNode} from "../../domain/sensor_node";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  private sensorNodes : SensorNode[] = [];
  private subscription : Subscription;
  private className : string;
  constructor(private nodeService : SensorNodeService) { }

  ngOnInit() {
    this.subscription = this.nodeService.getData().subscribe(data => {
      this.sensorNodes = data;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  selectStatusClass(node: SensorNode) : string{
    if(node.status == "DOWN"){
      return "label label-danger";
    }else if (node.status == "INTERMITTENT FAILURE"){
      return "label label-warning";
    }else {
      return "label label-success";
    }
  }

  selectConnectionClass(node: SensorNode) : string{
    return  node.connectionStatus == "CONNECTED" ? "label label-success" : "label label-danger"
  }


}
