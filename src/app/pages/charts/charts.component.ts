import {Component, OnInit, OnDestroy, ViewChild, AfterViewInit} from "@angular/core";
import {SensorNodeService} from "../../services/node_service";
import {SensorNode} from "../../domain/sensor_node";
import {GraphData} from "../../domain/graph_data";
import {ChartComponent} from "angular2-chartjs";
import {GraphComponent} from "../../directives/graph/graph.component";
var moment = require("moment");
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(ChartComponent) chartComponent: ChartComponent;
  public sensorNodes : any[] = [];
  public graphData : GraphData = new GraphData();
  public subscriptions = [];
  public sensorNode1: SensorNode = new SensorNode();
  public sensorNode2: SensorNode = new SensorNode();
  public period: any = null;
  public periods = ['Hour', 'Day', 'Week', 'Month', 'Year'];
  public type = 'bubble';
  public type_2 = 'bar';
  public data = {
    datasets: [{
      label: 'Scatter Dataset',
      data: [
      ],
      backgroundColor:"#1bccff",
    },{
      label: 'Scatter Dataset 2',
      data: [
      ],
      backgroundColor:"#ff7711",
    }]
  };
  public data_2 = {
    labels : [],
    datasets: [{
      label: 'Bar Chart Dataset',
      data: [
      ],
      backgroundColor:"#ff7711",
    }]
  };
  public options = {
    elements: {
      points: {
        borderWidth: 1
      }
    },
    title: {
      display: true,
      text: 'Scatter Plot'
    }
  };

  public options_2 = {
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          unit : 'hour',
          displayFormats: {
            'hour': 'YYYY-MM-DD-HH:mm:ss',
            'minute': 'YYYY-MM-DD-HH:mm:ss',
          }
        }
      }],
    }
  };

  public sensor_data = [
    {
      "type": "LIGHT",
      "reading": 5.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-13:09:44"
    },
    {
      "type": "LIGHT",
      "reading": 4.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-14:09:44"
    },
    {
      "type": "LIGHT",
      "reading": 12.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-15:09:44"
    },
    {
      "type": "LIGHT",
      "reading": 6.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-16:09:44"
    },
    {
      "type": "LIGHT",
      "reading": 1.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-17:09:44"
    },
    {
      "type": "LIGHT",
      "reading": 22.8921475410461,
      "unit": "VOLT",
      "timestamp": "2017-03-26-18:09:44"
    }
  ];

  constructor(private sensorNodeService : SensorNodeService) {
  }

  ngOnInit() {
    let subscription = this.sensorNodeService.getData().subscribe(data => {
      this.sensorNodes = data;
      console.log(data);
    });
    this.subscriptions.push(subscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(function (item) {
      item.unsubscribe();
    });
  }

  compareNodes(){
    let graph = new GraphData();
    graph.label = "Bar Chart";
    let labels = [];
    let points = [];
    console.log(this.sensorNode1.name + " - " + this.sensorNode2.name + " - " + this.period);
    let subscription = this.sensorNodeService.getDataFromNode(this.sensorNode1,this.period).subscribe(data => {
      console.log(data);
      graph.size = data.length;
      data.forEach(function (item) {
        labels.push(item.timestamp);
        points.push(item.reading);
      });
    });

    this.subscriptions.push(subscription);
    graph.data_labels = labels;
    graph.data_points = points;
    this.graphData = Object.assign(graph);
    this.chartComponent.chart.update();
  }

  ngAfterViewInit(): void {
    this.compareNodes();

  }
}
