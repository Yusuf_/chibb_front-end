import {Component, OnInit, ViewChildren, QueryList} from "@angular/core";
import {ChartComponent} from "angular2-chartjs";
import * as moment from "moment";
import {SensorNodeService} from "../../services/node_service";
import {SensorNode} from "../../domain/sensor_node";
declare var CanvasJS: any;

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {
  @ViewChildren(ChartComponent) chartComponent: QueryList<ChartComponent>;

  type = 'bubble';
  data = {
    labels: [],
    datasets: [
      {
        label: 'Scatter Dataset',
        data: [],
        backgroundColor:"#1bccff",
      },
      {
        label: 'Scatter Dataset 2',
        data: [],
        backgroundColor:"#ff7711",
      }
    ]
  };
  options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          unit : 'minute',
        }
      }],
    }
  };

  bar_type = 'bar';
  bar_data = {
    labels: [],
    datasets: [
      {
        label: 'Bar Chart',
        data: [],
        backgroundColor:"#1bccff",
      }
    ]
  };
  bar_options = {
    responsive: true,
    maintainAspectRatio: false
  };

  barChart(){
    let node = new SensorNode();
    node.id = '1';
    node.name = 'Fake Shit';
    let period : any = 'day'

    let labels = [];
    let points : number[] = [];
    let subscription = this.sensorNodeService.getDataFromNode(node,period).subscribe(data => {
      console.log(data);
      data.forEach(function (item) {
        labels.push(item.timestamp);
        points.push(item.reading);
      });
      console.log(points);
      this.bar_data.datasets[0].data = points;
      this.bar_data.labels = labels;
      this.chartComponent.forEach(function (item) {
        item.chart.update();
      });
    });
  }

  generateRandomData() {
    let points = [];
    let points2 = [];

    let node = new SensorNode();
    node.id = '1';
    node.name = 'Fake Shit';

    let node2 = new SensorNode();
    node2.id = '2';
    node2.name = 'Real Shit';

    let period : any = 'day'
    let subscription = this.sensorNodeService.getDataFromNode(node,period).subscribe(data => {
      console.log(data);
      for(let i = 0; i < data.length; i++){
        let point = {
          x: moment(data[i].timestamp,'YYYY-MM-DD HH:mm:ss'),
          y: data[i].reading,
          r : 10
        };
        console.log(point)
        points.push(point);
      }
      this.data.datasets[0].data = points;
      this.data.datasets[0].label = node.name;
    });

    let subscription2 = this.sensorNodeService.getDataFromNode(node2,period).subscribe(data => {
      console.log(data);
      for(let i = 0; i < data.length; i++){
        let point = {
          x: moment(data[i].timestamp,'YYYY-MM-DD HH:mm:ss'),
          y: data[i].reading,
          r : 10
        };
        console.log(point)
        points2.push(point);
      }
      this.data.datasets[1].data = points2;
      this.data.datasets[1].label = node2.name;
      this.chartComponent.forEach(function (item) {
        item.chart.update();
      });      //this.chartComponent.chart.update();
    });

  }
  constructor(private sensorNodeService : SensorNodeService) { }


  ngOnInit() {
  }

}
