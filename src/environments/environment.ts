// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
let url : string = "145.24.222.65";
let address = "http://" + url + ":";
export const environment = {
  production: false,
  server  : {
    temperature : {
      api : address + '9002',
      socket: 'ws://' + url + ':9004/chibb-home'
    },
    discovery : {
      url : address + '9005/services/'
    },
    sensor_nodes : {
      url : address + 9004
    }
  }
};
