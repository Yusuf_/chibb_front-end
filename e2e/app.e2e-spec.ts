import { ChibbIctlabPage } from './app.po';

describe('chibb-ictlab App', function() {
  let page: ChibbIctlabPage;

  beforeEach(() => {
    page = new ChibbIctlabPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
